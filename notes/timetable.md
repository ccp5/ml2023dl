# Timetable

The Autumn school will start at 12 noon on 18th Sept and run til 2pm on 20th September.

!!! danger "poster"

    If you are bringing a poster for the poster session on the 19th the poster boards will be setup as A0 portrait. You must print and
    bring your poster with you.


| **Monday 18 September** |                                                                                                                                              |
|---------------------|----------------------------------------------------------------------------------------------------------------------------------------------|
| 12:00 -- 13:00      | Registration & Lunch                                                                                                                         |
| 13:00 -- 13:30      | Welcome & Introduction                                                                                                                       |
| 13:30 -- 15:00      | [Lecture Session: Basic introduction to ML topics – Reinhard Maurer](pdfs/MLIntro.pdf)            |
| 15:00 -- 15:30      | *Coffee*                                                                                                                                     |
| 15:30 -- 17:15      | Practical session: Basic ML worked example – Reinhard Maurer|
| 17:30 -- 18:30      | Research Seminar – Aron Walsh                                                                                               |
| 19:00 -- 21:00      | *BBQ*

| **Tuesday 19 September** |                                                                                                                                                                            |
|----------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 09:00 -- 10:30       | [Lecture: Machine Learning Interatomic Potentials – Ioan Magdău](pdfs/MLIP.pdf) |
| 10:30 -- 11:00       | *Coffee*                                                                                                                                                                   |
| 11:00 -- 12:30       |  Practical Session MLIP  – Ioan Magdău + Alin-Marin Elena                                                                           |
| 12:30 -- 14:00       | *Lunch*                                                                                                                                                                    |
| 14:00 -- 15:30       |  Practical Session MLIP – Ioan Magdău + Alin-Marin Elena                                       |
| 15:30 -- 16:00       | *Coffee*                                                                                                                                                                   |
| 16:00 -- 17:30       |  [Lectures: GNN talks – Keith Butler + Alex Ganose](pdfs/neural-nets.pdf) [Alex](pdfs/Intro-to-GNNs.pdf)                                                                                                                               |
| 18:00 -- 20:00       | *Poster session & aparetif & pizza*                                                                                                                        |


| **Wednesday 20 September** |                      |
|---------------------|-------------------------------|
| 09:00 -- 10:30      | Practical Session: Building and training GNN – Keith Butler & Alex Ganose |
| 10:30 -- 11:00      | *Coffee*                      |
| 11:00 -- 12:30      | Practical Session: Using pre trained networks – Keith Butler & Alex Ganose  |
| 12:30 -- 14:00      | Lunch (optional)&Departure    |
