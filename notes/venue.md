# Venue&Arrival

The workshop is taking place at [Daresbury Laboratory](https://www.sci-techdaresbury.com/science-facilities/daresbury-laboratory/). The nearest rail connections are in Warrington and Runcorn. Attendees are responsible for sorting their own transport to the venue.

On arrival, make your way to the [Daresbury Laboratory general reception](https://goo.gl/maps/8G7mvLJsJWBSBSZeA) or [///cake.noisy.baked](https://w3w.co/cake.noisy.baked) ; the workshop organisers will meet you there.

If you are travel from Runcorn train station, best way is by bus number 200 or by taxi local company. If you travel from Warrington train stations the best travel is by taxi to come to the laboratory. During the week, you can come to the lab by taxi (you can use abba cars 01925 44 44 44) or by bus X30 Warrington bus station is close to your hotel.

!!! success "evening transport"

    In the evenings we will arrange for group transport by Taxi from the laboratory to the hotel.

!!! warning "parking"

    If you are arriving by car and need to park at Daresbury Laboratory, please provide the car registration number to  [SCD Events](mailto:SCD_Events@stfc.ac.uk)



## Travelling to Daresbury Laboratory:

### Train

- Runcorn East Station - Barnfield Ave, Norton, Cheshire, Runcorn WA7 6EP

- Warrington Bank Quay - Parker Street, Warrington, CheshireWA1 1LW

### Bus

- X30 - Operated by Arriva

This service operates hourly between Chester and Warrington. The service offers 3 morning buses from Warrington Interchange, departing at 06:58, 08:00 and 09:00 to arrive on campus at 07:14, 08:16 and 09:16, respectively. Two evening services will operate from campus to Warrington Interchange, departing at 16:53 and 17:58 to arrive at 17:12 and 18:17, respectively.

[View full timetable](https://www.arrivabus.co.uk/find-a-service/x30-chester-to-warrington)

- 200 - Operated by Arriva

This service offers a circular route between Widnes, Runcorn (including both train stations) and Sci-Tech Daresbury. Generally, there’s one service per hour with some additional peak period services.

[View full timetable](https://www.arrivabus.co.uk/find-a-service/200-runcorn-industrial-circular)

### Local taxi companies

- ABBA Cars - 01925 444444 - warrington

- APEC Taxis - 01928 575757 - runcorn

### Plane

if you come by plane and land at Manchester or Liverpool airports best is to prebook a taxi. Call one of the companies above. ABBA
for Manchester, APEC for Liverpool.
