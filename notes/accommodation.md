# Accommodation


2 nights accommodation is provided (18th and 19th September) at [Travelodge Warrington](https://www.travelodge.co.uk/hotels/189/Warrington-hotel). If you require accommodation the night before the school you will need to arrange this yourself.


Breakfast on the 19th and 20th September will be provided at the hotel.

Vouchers will be provided for lunch in the Daresbury canteen each day.

On the evening of the 18th and 19th Dinner will be provided at the laboratory.

!!! danger "dinner"

    If you have any dietary requirements that you did not include on your registration form please contact [SCD Events](mailto:SCD_Events@stfc.ac.uk)

