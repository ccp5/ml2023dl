# About


![some pics](pics/dl2023.png){ loading=lazy }

This page contains some information for attendees of the "Machine Learning for Atomistic Modelling Autumn School 2023" held at Daresbury Laboratory, UK from September 18-20 2023.


# organising committee

- Alin-Marin Elena, Scientific Computing Department STFC
- Keith Butler, Queen Mary University London
- Reinhard Maurer, University of Warwick
- Kim Jelfs, Imperial College London
- Alex Ganose, Imperial College London
- Simon Coles, University of Southampton
- Samantha Kanza, University of Southampton
- Nicola Knight, University of Southampton


# sponsors


![some pics](pics/sponsors.png){ loading=lazy }
